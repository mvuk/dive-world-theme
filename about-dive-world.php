<?php
/*
Template name: About Dive World
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row fullWidth slider">
	<div class="about-slider about-dive-world">

		<div class="nav-container">
			<div class="navigation">
				<div class="item-active">
					<a href="/about-us/about-dive-world/">About Dive World</a>
				</div>
				<div class="item">
					<a href="/about-us/meet-the-owner/">Meet the Owner</a>
				</div>
				<div class="item">
					<a href="/about-us/meet-training-director/">Meet the Training Director</a>
				</div>
				<div class="item">
					<a href="/partners/">Partners</a>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="row about-content">
	<div class="large-9 column">

		<div class="row title-row">
			<div class="large-12 column">
				<h1>About Dive World</h1>
			</div>
		</div>

		<div class="row content-row">
			<div class="large-12 column">
				<h2>Mandate</h2>
				<p>Dive World is not just a dive shop – we are a resource for divers, snorkelers, spear fishermen, free divers, travelers and marine lovers. Our goal is to facilitate an environment that inspires people to care more about the marine ecosystems and to equip people with the right skills, gear and information to gain access to the beauty of the underwater world. Our hope is that by increasing people’s exposure to marine habitats and the animals that live in them, we will inspire and compel people to invest their time and money in the protection of these valuable ecosystems.</p>
			</div>
			<!-- <div class="row">
				<div class="large-12 column">
					<img src="/wp-content/uploads/2018/08/dive-world-gullivers-group.jpg" alt="" class="featured-margin">
				</div>
			</div> -->
			<div class="row">
				<div class="large-6 columns">
					<img src="/wp-content/uploads/2018/08/IMG_9334.jpg" alt="" class="featured-margin">
				</div>
				<div class="large-6 columns">
					<img src="/wp-content/uploads/2018/08/IMG_9319.jpg" alt="" class="featured-margin">
				</div>
			</div>
			<div class="large-12 column">
				<h2>About our school</h2>
				<p>If you don’t already know how to snorkel or dive we are here to help. We offer a wide range of courses from beginner to professional to help build your confidence incrementally and develop the kind of comfort and familiarity with water in order to maximize your enjoyment and stimulate your curiosity and love of the ocean.</p>
				<p>It has been our experience that most people who don’t already have a love of snorkeling or diving feel this way because they have yet to be exposed to the kind of passionate people that we have at Dive World. We are not here to just certify divers, we are here to teach and share our love for diving.</p>
			</div>
			<div class="row">
				<div class="large-6 columns">
					<img src="/wp-content/uploads/2018/08/dive-world-courses-hand-shake.jpg" alt="" class="featured-margin">
				</div>
				<div class="large-6 columns">
					<img src="/wp-content/uploads/2018/08/dive-world-courses-buoyancy-fins.jpg" alt="" class="featured-margin">
				</div>
			</div>
			<div class="large-12 column">
				<h2>About our store</h2>
				<p>Another big factor in being able to enjoy the marine environment is having the right equipment for a safe and comfortable experience in the water. At Dive World we don’t simply sell you gear, we take the time to explain why certain gear is right for you and we teach you how to properly fit yourself for the right equipment that we guarantee will make your aquatic experience much more rewarding. Having the right gear makes all the difference.</p>
			</div>
			<div class="row">
				<div class="large-12 column">
					<img src="/wp-content/uploads/2018/08/dive-world-shop-indoor-demonstration-1.jpg" alt="">
				</div>
			</div>
			<div class="large-12 column">
				<h2>A few simple principles</h2>
				<ul>
					<li>We never turn anyone away. It doesn’t matter if you don’t plan on buying anything from our store, we will still gladly take the time to teach you how to fit yourself, size yourself and educate you on the advantages and disadvantages of different materials and features.</li>
					<li>We understand that not everyone has a natural predisposition towards water, or the same starting set of skills. We understand that not everyone has the same physiology or fitness level; people come in all shapes and sizes. We want to empower people to have the skills and confidence to go out and enjoy nature, to interact with it, and fall hopelessly in love with it to the point that they feel the need to help it.</li>
				</ul>
				<p>In short, we are here to maximize service, not profit. Of course we need to make money but we developed this business model to lower our operational costs to ensure that the pressures of paying our bills never interfere with what is more important: NEVER compromising on quality and service for the people who come to us.</p>
				<p>We are people that are rooted in compassion for the marine environment. Thus we feel strongly that by empowering people with the confidence to enjoy rivers, lakes, seas and oceans we can magnify our positive impact from a conservation standpoint. Our customers are our assets because the more confident divers we create and the more incredible marine interaction we facilitate, the more we will create motivated word of mouth advocates for marine protection.</p>
			</div>
			<div class="row">
				<!-- <div class="large-6 columns">
					<img src="/wp-content/uploads/2018/08/1U0A0372.jpg" alt="">
				</div> -->
				<div class="large-12 column">
					<img src="/wp-content/uploads/2018/08/IMG_9466.jpg" alt="">
				</div>
			</div>
		</div>

	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
