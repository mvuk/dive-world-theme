<?php
/*
Template name: AIDA 3
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div  class="page-wrapper page-left-sidebar">
<div class="row">

<div id="content" class="large-9 right columns course-template" role="main">
	<div class="page-inner">
			<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template();
					?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="large-12 column">
	<img src="/wp-content/uploads/2017/10/geoff-coombs-freediving-2.jpg" alt="" class="marginbottom25">
</div>

<div class="large-12 column">
	<!-- <div class="large-12 columns video-intro">
		<iframe src="https://www.youtube.com/embed/tWPGi52nkHg" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
	</div> -->
	<p>The AIDA 3 is for people who have experience diving. Completing the AIDA 2 course is a
prerequisite to the AIDA 3 certification. This course will instrumental in bringing all aspects of your
freediving abilities to an advanced level.</p>
	<ul>
		<li>Improve your technique and confidence to dive longer, deeper, and more comfortably. Learn
how to dive safely with the difficulties and hazards from water pressure at increased depths.</li>
		<li>Learn how to plan and implement specific training routines to use with a similarly experienced
buddy to build your conditioning for freediving during training sessions.</li>
		<li>Obtain an in-depth understanding of freediving concepts and physiology. This course explores
deeper into the concepts introduced in AIDA 2, as well as many new concepts.</li>
	</ul>
</div>

<div class="large-12 column">
	<h4>Course Structure over four days:</h4>
	<ul>
		<li>Three theory sessions</li>
		<li>Two pool sessions</li>
		<li>Four open water sessions</li>
	</ul>
	<h4>Course requirements to be qualify for certification:</h4>
	<ul>
		<li>55m distance underwater (DYN)</li>
		<li>2:45 stationary breath hold time in water (STA)</li>
		<li>24m depth (CWT)</li>
		<li>One final exam, with a minimum passing score of 75%</li>
	</ul>
</div>

<div class="large-6 columns">
	<h4>Cost:</h4>
	<p>The full price of the AIDA 3 course is $700, covering the following:</p>
	<ul>
		<li>All course materials (AIDA 3 manual, presentation slides, exams)</li>
		<li>AIDA certification fees</li>
		<li>Pool entrance fees</li>
		<li>Low student-to-instructor ratios</li>
	</ul>
	<h4>What to bring:</h4>
	<p>Students are expected to bring the following:</p>
	<ul>
		<li>Mask</li>
		<li>Snorkel</li>
		<li>Fins</li>
		<li>Proper wetsuit for open water diving</li>
		<li>Gloves and socks for open water diving</li>
		<li>Weight belt with weights</li>
		<li>Dive computer</li>
	</ul>
	<p>Please contact Diveworld for rental options at a special AIDA 3 rate. Students will be responsible for transportation between locations, although carpooling with other students or the instructor could be arranged if needed.</p>
</div>
<div class="large-6 columns">
	<img src="/wp-content/uploads/2017/10/geoff-coombs-freediving-4.jpg" alt="" class="feature-ps-one">
</div>

<!-- START CHECKOUT ELEMENT -->
<div class="large-12 columns aow-final">
	<h2>Register for AIDA 3 Freediving</h2>
	<p>Register below and we will walk you through what’s next.</p>
	<div class="large-12 columns ticket-selector-div">
		<div class="ticket-selection">
				<div class="pseudoCourseRegDiv">
					<a href="http://diveworld.ca/courses/aida-3-freediving/aida-3-checkout/" class="a-pseudoCourseRegDiv">Register Now</a>
				</div>
		</div>
		<div class="large-6 columns ssl">
			<div class="large-12 columns">
				<div class="large-4 small-4 columns">
					<div class="ssl-img"></div>
				</div>
				<div class="large-8 small-8 columns">
					<p>Your payment to Dive World Inc. is secured with an SSL Certificate by GeoTrust</p>
				</div>
			</div>
		</div>
		<div class="large-6 columns secure-payment">
			<div class="large-12 columns">
				<p>Secure Payment Options <i class="fa fa-lock" aria-hidden="true"></i></p>
				<div class="secure-payment-img"></div>
			</div>
		</div>
	</div>
</div>
<!-- END CHECKOUT ELEMENT -->



<!-- PAGE CONTENT ENDS HERE -->

	</div><!-- .page-inner -->
</div><!-- end #content large-9 left -->

<div class="large-3 columns left">
<?php get_sidebar(); ?>
</div><!-- end sidebar -->

</div><!-- end row -->
</div><!-- end page-right-sidebar container -->


<?php get_footer(); ?>
