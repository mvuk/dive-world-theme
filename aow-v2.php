<?php
/*
Template name: Advanced Open Water v2
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<!-- START SLIDER -->

<div class="sliderArea aow">
	<div class="large-12 columns">
		<div class="sliderInner">
			<h1>PADI Advanced Open Water Diver</h1>
			<p>The easiest, most flexible way to gain experience as a scuba diver.</p>
			<ul>
				<li>Develop new skills</li>
				<li>Dive deeper</li>
			</ul>
			<a href="#" class="btn">Learn More</a>
		</div>
	</div>
</div>

<!-- END SLIDER -->

<div  class="page-wrapper page-left-sidebar aow-wrapper">
<div class="row">

<div id="content" class="large-12 right columns" role="main">
	<div class="page-inner">

<!-- PAGE CONTENT STARTS HERE -->

<div class="large-12 columns reviewers">
	<div class="reviewsText">
		<p>100% of reviewers recommend Dive World</p><img src="/wp-content/uploads/2015/07/yelp-5-stars.png" alt="Five Star Reviews">
	</div>
</div>

<div class="large-12 columns needtoknow">
	<div class="large-12 columns">
		<h2>Here’s what you need to know</h2>
	</div>
	<div class="large-4 columns">
		<img src="" alt="">
		<h3>Fun Only</h3>
		<p>No classrooms, no exams - just bubbles.</p>
	</div>
	<div class="large-4 columns">
		<img src="" alt="">
		<h3>Access</h3>
		<p>By completing this training you receive access to a much larger range of dive sites.</p>
	</div>
	<div class="large-4 columns">
		<img src="" alt="">
		<h3>Real Skills</h3>
		<p>Maximize safety will skills that apply to the real world.</p>
	</div>
</div>

<div class="large-12 columns pricing">
	<div class="large-12 columns">
		<h2>Ready to Dive?</h2>
		<p>Our flexible pricing options make it easy to get started.</p>
	</div>
	<div class="large-4 columns priceBox">
		<div class="priceQuarter">
			<h3>Express</h3>
			<p class="price">$395</p>
			<p class="point">2 days</p>
			<p class="point">5 dives</p>
			<p class="point">All course materials</p>
			<p class="point">AOW license</p>
			<a href="#" class="registerNow">Register Now</a>
		</div>
	</div>
	<div class="large-4 columns priceBox">
		<div class="priceQuarter">
			<h3>Standard</h3>
			<p class="price">$425</p>
			<p class="point">3 days</p>
			<p class="point">5 dives</p>
			<p class="point">All course materials</p>
			<p class="point">AOW license</p>
			<a href="#" class="registerNow">Register Now</a>
		</div>
	</div>
	<div class="large-4 columns priceBox">
		<div class="priceQuarter">
			<h3>Recommended</h3>
			<p class="price">$475</p>
			<p class="point">3 days</p>
			<p class="point">6 dives</p>
			<p class="point">All course materials</p>
			<p class="point">AOW license</p>
			<a href="#" class="registerNow">Register Now</a>
		</div>
	</div>
</div>

<div class="large-12 columns finePrint">
	<div class="finePrintDiv">
		<p>*All prices listed are for certified divers who have their own equipment. If you need to rent equipment Dive World has top of the line, brand new equipment available for rent at special rates for our program participants.</p>
		<p>**For divers looking to purchase their own equipment Dive World has special pricing for program participants.</p>
		<p>*** If you own your own equipment that you have not used in a long time, this is the perfect opportunity to have a professional look it over and give you some input. We are here to help!</p>
	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

	</div><!-- .page-inner -->
</div><!-- end #content large-9 left -->

</div><!-- end row -->
</div><!-- end page-right-sidebar container -->


<?php get_footer(); ?>
