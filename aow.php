<?php
/*
Template name: Advanced Open Water
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div  class="page-wrapper page-left-sidebar">
<div class="row">

<div id="content" class="large-9 right columns" role="main">
	<div class="page-inner">
			<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template();
					?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="large-12 columns">
	<img src="/wp-content/uploads/2018/08/diveworld-advanced-open-water-course.jpg" alt="" class="featured-margin">
	<h2>Who should take this course?</h2>
	<div class="large-12 columns">
		<p>If you are a newly certified diver, or you have limited experience in different types of diving, then this is the perfect program for you.</p>
		<p>The advanced open water course allows divers to get a light introduction into five different types of dives. The purpose of these dives is to help divers become more well-rounded and experienced, while at the same time helping you to determine which specialties you would want to focus on.</p>
	</div>
</div>

<div class="large-12 columns">
  <h2>How does the course work?</h2>
  <p class="kr-theory-p">The PADI Advanced Open Water course consists of two parts:</p>
  <div class="large-12 columns how-works-reason">
    <h3>1. Knowledge Development (Theory Component)</h3>
    <p>Complete online theory to review the unique aspects of the 5 adventure dives you will be conducting during the course.</p>
  </div>
  <div class="large-12 columns how-works-reason">
    <h3>2. Five Adventure Dives (Fresh Water Training)</h3>
    <p>Complete five adventure dives and receive your Advanced Open Water certification. There are two required dives and three elective dives.</p>
  </div>
</div>

<div class="large-12 columns kd-option-div">
	<div class="large-12 columns kd-options reccomended elearning">
		<div class="large-12 columns content">
			<div class="large-4 columns img">
				<div class="img"></div>
			</div>
			<div class="large-8 columns">
				<h3>$200 - Adventures in Diving Certification Pak</h3>
				<p>Included: Adventures in Diving eLearning Certification Course Materials</p>
				<p>Theory review with a Dive World Pro.</p>
				<p>Non Expiring International PADI Advanced Open Water License.</p>
			</div>
		</div>
	</div>

	<!-- <div class="large-12 columns kd-options manual-dvd">
			<div class="large-4 columns img">
				<div class="img"></div>
			</div>
			<div class="large-8 columns">
				<h3>Manual & DVD $155</h3>
				<p>If you prefer to learn using a manual and DVD with classroom instruction instead of computer or tablet-based learning we offer that too!</p>
				<p><strong>Includes: PADI Adventures In Diving Manual & Data Carrier Slate</strong></p>
				<p>The Adventures in Diving Manual can be picked up at Dive World during store hours or can be shipped directly to you for an additional shipping cost. You are required to watch the Adventures in Diving video and review the knowledge review answers with your instructor.</p>
				<p>Includes $65 PADI Advanced Open Water Diver license</p>
			</div>
	</div> -->
</div>

<!-- Includes $55 PADI Advanced Open Water Diver license -->


<div class="large-12 columns adventure-dives">

	<h2>Adventure Dives $250</h2>
	<p class="kr-theory-p">Complete five adventure dives and receive your Advanced Open Water certification. There are two required dives and three elective dives.</p>

	<div class="large-12 columns a-dive">
		<h3>Deep Dive (Mandatory)</h3>
		<p>We will learn how to safely plan and execute a deep dive to a maximum depth of 30m (100ft) paying special attention to how depth impacts gas consumption, narcosis, buoyancy, allowable bottom time, light and temperature. We will also learn how to deal with emergency decompression. This is Dive 1 of the Deep Specialty Course.</p>
		<p><strong>Mandatory gear:</strong> Dive Computer - if you don't already own a dive computer this would be a good time to invest in one so your instructor can teach you how to use it. Available for rent $10/day.</p>
	</div>

	<div class="large-12 columns a-dive">
		<h3>Navigation Dive (Mandatory)</h3>
		<p>Visibility is a huge variable in every dive you do. Learning how to use a compass with precision is of utmost importance to developing true comfort underwater. We will learn how to use time, kick cycles, natural navigation and a compass to maximize situational awareness while we dive. This is Dive 1 of the Navigation Specialty Course.</p>
		<p><strong>Mandatory gear:</strong> Compass -  if you don't already own a compass this would be a good time to invest in one so your instructor can teach you how to use it. Our rental regulators come equipped with compasses. Compasses are also available for rent for $10/day.</p>
	</div>

	<div class="large-12 columns a-dive">
		<h3>Peak Performance Buoyancy Dive (Elective)</h3>
		<p>Buoyancy is the most important skill in diving. During the PPB Dive we will learn how to precisely weight ourselves based on environmental and equipment considerations. We work on buoyancy fundamentals like controlled ascents, descents and how body and weight position affect the way we float. This is Dive 1 of the Peak Performance Buoyancy Specialty Course.</p>
	</div>

	<div class="large-12 columns a-dive">
		<h3>Night Dive (Elective)</h3>
		<p>We will learn how to safely plan and execute a dive at night paying special attention to unique safety procedure, environmental orientation, underwater communication and hand signals. This is Dive 1 of the Night Diver Specialty Course.</p>
		<p><strong>Mandatory gear:</strong> Underwater flashlight - If you don't already own a flashlight we recommend investing in one as they are very useful even on day dives. Underwater lights are available for rent for $15/day.</p>
	</div>

	<div class="large-12 columns a-dive">
		<h3>Search & Recovery Dive (Elective)</h3>
		<p>We will learn how to use different search patterns to find lost objects underwater. Once found we will learn how to safely secure and bring these objects to the surface by using different knots, ropes and lift bags. This is Dive 1 of the Search & Recovery Specialty Course.</p>
		<p><strong>Mandatory gear:</strong> Compass -  if you don't already own a compass this would be a good time to invest in one so your instructor can teach you how to use it. Our rental regulators come equipped with compasses. Compasses are also available for rent for $10/day.</p>
	</div>

	<div class="large-12 columns a-dive">
		<h3>Wreck Dive (Elective)</h3>
		<p>We will learn how to safely execute a wreck dive by paying special attention to orientation, hazards, historical significance and buddy procedure. This is Dive 1 of the Wreck Diver Specialty Course.</p>
		<p><strong>Mandatory gear:</strong> Compass -  if you don't already own a compass this would be a good time to invest in one so your instructor can teach you how to use it. Our rental regulators come equipped with compasses. Compasses are also available for rent for $10/day.</p>
	</div>

	<div class="large-12 columns a-dive">
		<h3>Full Face Mask Dive (Elective)</h3>
		<p>We will execute a dive wearing a full face scuba mask paying special attention to emergency procedures, how to deal with out of air situations, and what the different features of a full face mask are used for.</p>
		<p><strong>Mandatory gear:</strong> Full-face scuba mask - Full face masks as available for rent for $75/dive or for sale at special rates for program participants.</p>
	</div>

	<div class="large-12 columns a-dive last">
		<h3>Delayed Surface Marker Buoy & Drift Dive (Elective)</h3>
		<p>Planning drift dives can be logistically difficult and requires experience and familiarity with the dive sites. On this dive we learn the fundamentals of executing a successful drift dive, how to properly plan and execute good buddy procedure and how to deploy surface markers from depth to avoid losing contact with the boat.</p>
		<p><strong>Mandatory gear:</strong> Surface marker & finger reel - This is a fundamental safety piece of equipment that we strongly recommend all divers invest in. Available for rent for $25/day.</p>
	</div>

</div>

<div class="large-12 column">
<p>*All prices listed are for certified divers who have their own equipment. If you need to rent equipment Dive World has top of the line, brand new equipment available for rent at special rates for our program participants.</p>
<p>**For divers looking to purchase their own equipment Dive World has special pricing for program participants.</p>
<p>*** If you own your own equipment that you have not used in a long time, this is the perfect opportunity to have a professional look it over and give you some input. We are here to help!</p>
</div>

<!-- START CHECKOUT ELEMENT -->
<div class="large-12 columns aow-final">
	<h2>Register for PADI Advanced Open Water Course</h2>
	<p>Register below to get started with the Knowledge Development (Theory) portion of the PADI Advanced Open Water Course.</p>
	<div class="large-12 columns ticket-selector-div">
		<div class="ticket-selection">
				<div class="pseudoCourseRegDiv">
					<a href="https://diveworld.ca/courses/padi-advanced-open-water/padi-advanced-open-water-checkout/" class="a-pseudoCourseRegDiv">Register Now</a>
				</div>
		</div>
		<div class="large-6 columns ssl">
			<div class="large-12 columns">
				<div class="large-4 small-4 columns">
					<div class="ssl-img"></div>
				</div>
				<div class="large-8 small-8 columns">
					<p>Your payment to Dive World Inc. is secured with an SSL Certificate by GeoTrust</p>
				</div>
			</div>
		</div>
		<div class="large-6 columns secure-payment">
			<div class="large-12 columns">
				<p>Secure Payment Options <i class="fa fa-lock" aria-hidden="true"></i></p>
				<div class="secure-payment-img"></div>
			</div>
		</div>
	</div>
</div>
<!-- END CHECKOUT ELEMENT -->

<!-- PAGE CONTENT ENDS HERE -->

	</div><!-- .page-inner -->
</div><!-- end #content large-9 left -->

<div class="large-3 columns left">
<?php get_sidebar(); ?>
</div><!-- end sidebar -->

</div><!-- end row -->
</div><!-- end page-right-sidebar container -->


<?php get_footer(); ?>
