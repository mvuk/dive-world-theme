<?php
/*
Template name: april 2018 clearance
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="april-clearance">

	<div class="row">
		<div class="large-12 column">
			<h1>Clearance!</h1>
			<p class="tagline">All items are in 100% new condition. These are not used goods. Sales are final.</p>
		</div>
	</div>
	<div class="row">
		<div class="large-3 columns">
			<a href="https://diveworld.ca/product/oceanic-viper-full-foot-fins/" class="clearance-a">
				<div class="ca-product">
					<img src="/wp-content/uploads/2018/04/oc_viper_fullfoot_blueyellow_top_web_1-e1509392751225-1.jpg" alt="">
					<h2>Oceanic Viper Full Foot Fins</h2>
					<p class="msrp">MSRP</p>
					<p class="msrp-amt">$64.95</p>
					<p class="saleprice">Sale Price</p>
					<p class="saleprice-amt">$25.00</p>
					<div class="shop-now-btn">
						Buy now
					</div>
				</div>
			</a>
		</div>

		<div class="large-3 columns">
			<a href="/product/oceanpro-durastretch-gloves-5mm-large/" class="clearance-a">
				<div class="ca-product">
					<img src="/wp-content/uploads/2018/05/51fgiFOGQSL._SL1000_.jpg" alt="">
					<h2>OceanPro DuraStretch Gloves – 5mm (Large)</h2>
					<p class="msrp">MSRP</p>
					<p class="msrp-amt">$69.95</p>
					<p class="saleprice">Sale Price</p>
					<p class="saleprice-amt">$35.00</p>
					<div class="shop-now-btn">
						Buy now
					</div>
				</div>
			</a>
		</div>


		<div class="large-3 columns">
			<a href="https://diveworld.ca/product/bare-7mm-cold-water-boots-size-12/" class="clearance-a">
				<div class="ca-product">
					<img src="https://diveworld.ca/wp-content/uploads/2018/04/cold.jpg" alt="">
					<h2>Bare 7mm Cold Water Boots Size 12</h2>
					<p class="msrp">MSRP</p>
					<p class="msrp-amt">$60.95</p>
					<p class="saleprice">Sale Price</p>
					<p class="saleprice-amt">$30.00</p>
					<div class="shop-now-btn">
						Buy now
					</div>
				</div>
			</a>
		</div>

		<div class="large-3 columns">
		</div>
		
					<!-- <div class="large-3 columns">
		<a href="https://diveworld.ca/product/sherwood-elite-fins-blackblack-size-11-12/" class="clearance-a">
			<div class="ca-product">
				<img src="https://diveworld.ca/wp-content/uploads/2018/04/SWDEF.png" alt="">
				<h2>Sherwood Elite Fins Black/Black Size 11-12</h2>
				<p class="msrp">MSRP</p>
				<p class="msrp-amt">$149.95</p>
				<p class="saleprice">Sale Price</p>
				<p class="saleprice-amt">$45.00</p>
				<div class="shop-now-btn">
					Buy now
				</div>
			</div>
		</a>
	</div> -->
	<!-- <div class="row">
		<div class="large-3 columns">
			<a href="https://diveworld.ca/product/akona-short-sleeve-rash-guard/" class="clearance-a">
				<div class="ca-product">
					<img src="https://diveworld.ca/wp-content/uploads/2015/12/AKNNRGS_3.png" alt="">
					<h2>Akona Short Sleeve Rash Guards</h2>
					<p class="msrp">MSRP</p>
					<p class="msrp-amt">$60.95</p>
					<p class="saleprice">Sale Price</p>
					<p class="saleprice-amt">$15.00</p>
					<div class="shop-now-btn">
						Buy now
					</div>
				</div>
			</a>
		</div> -->
		<!--  -->
		<!-- <div class="large-3 columns">
			<a href="https://diveworld.ca/product/akona-rash-guard-long-sleeve/" class="clearance-a">
				<div class="ca-product">
					<img src="https://diveworld.ca/wp-content/uploads/2016/02/AKNNRGLSSMBL.png" alt="">
					<h2>Akona Long Sleeve Rash Guards</h2>
					<p class="msrp">MSRP</p>
					<p class="msrp-amt">$69.95</p>
					<p class="saleprice">Sale Price</p>
					<p class="saleprice-amt">$15.00</p>
					<div class="shop-now-btn">
						Buy now
					</div>
				</div>
			</a>
		</div> -->
		<!--  -->

		<!-- <div class="large-3 columns">
			<a href="https://diveworld.ca/product/riffe-silent-hunter-fins/" class="clearance-a">
				<div class="ca-product">
					<img src="/wp-content/uploads/2018/04/FIN-0420.jpg" alt="">
					<h2>Riffe Silent Hunter Fins</h2>
					<p class="msrp">MSRP</p>
					<p class="msrp-amt">$329.95</p>
					<p class="saleprice">Sale Price</p>
					<p class="saleprice-amt">$75.00</p>
					<div class="shop-now-btn">
						Buy now
					</div>
				</div>
			</a>
		</div> -->
		<!--  -->
		<!-- <div class="large-3 columns">
			<a href="https://diveworld.ca/product/bare-5mm-sealtek-wet-hood/" class="clearance-a">
				<div class="ca-product">
					<img src="https://diveworld.ca/wp-content/uploads/2018/04/BREHS5.png" alt="">
					<h2>Bare 5mm Sealtek Wetsuit Hood Size XL</h2>
					<p class="msrp">MSRP</p>
					<p class="msrp-amt">$89.95</p>
					<p class="saleprice">Sale Price</p>
					<p class="saleprice-amt">$50.00</p>
					<div class="shop-now-btn">
						Buy now
					</div>
				</div>
			</a>
		</div> -->

	</div>


</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
