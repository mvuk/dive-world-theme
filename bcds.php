<?php
/*
Template name: shop/bcd
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div id="content" class="large-12 columns">
	<div class="mobile-cat">
		<div class="cat-title"><h1>BCDs</h1></div>

		<div class="cat-list">

			<div class="cat-item">
				<a href="https://diveworld.ca/product-category/scuba/buoyancy-control-devices-bcd/jacket-bcd/" class="cat-top-level"><h2>Jacket<i class="fa fa-chevron-right cat-right"></i></h2></a></div>
			<div class="cat-item">
				<a href="https://diveworld.ca/product-category/scuba/buoyancy-control-devices-bcd/back-inflation-bcd/" class="cat-top-level"><h2>Back Inflation<i class="fa fa-chevron-right cat-right"></i></h2></a>
			</div>
			<div class="cat-item">
				<a href="https://diveworld.ca/product-category/scuba/buoyancy-control-devices-bcd/side-mount/" class="cat-top-level"><h2>Side Mount<i class="fa fa-chevron-right cat-right"></i></h2></a>
			</div>
			<div class="cat-item">
				<a href="https://diveworld.ca/product-category/scuba/buoyancy-control-devices-bcd/parts-accessories/" class="cat-top-level"><h2>Parts & Accessories<i class="fa fa-chevron-right cat-right"></i></h2></a>
			</div>

		</div>

	</div>

</div>


<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
