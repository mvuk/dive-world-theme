<?php
/*
Template name: belize aggressor
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row tripRow top">
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2018/02/Belize-Image-1.jpg" alt="">
	</div>
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2018/02/Belize-Image-2.jpg" alt="">
	</div>
</div>

<div class="row tripIntro">
	<div class="large-12 column">
    <h1>BELIZE</h1>
		<p class="intro-dates">May 11th-18th 2019</p>
		<p class="trip-desc">Belize is a wall diver's paradise. The luxurious Belize Aggressor IV casts off from the Radisson Ft. George dock for a week of hassle-free diving to Lighthouse Reef, Half Moon Cay and the famous Blue Hole. Up to 5 dives a day scheduled inlcuding a night dive each evening.</p>
		<p class="trip-desc">All staterooms are air-conditioned with ensuite bathroom. Breakfast, buffet lunch and an elegant meal at dinner with tableside service, in addition to fresh mid-morning and mid-afternoon snacks. Beverages, soft drinks, local beer and wine and complimentary.</p>
		<!-- <a href="https://dw352.infusionsoft.com/app/orderForms/Dive-World-Sunset-House-Grand-Cayman-Feb-2019" class="registerOnline">Register Online</a> -->
	</div>
</div>

<div class="row tripRow">
	<div class="large-12 column">
		<img src="/wp-content/uploads/2018/11/diveworld-belize-aggressor-trip.jpg" alt="">
	</div>
</div>

<div class="row ">
	<div class="large-12 column">
		<div class="alt-cta-area">
			FOR QUESTIONS OR TO RESERVE YOUR SPOT PLEASE CALL SQUBA HOLIDAYS AT
			<span class="FooterNumber">1-800-265-3447</span>
		</div>
	</div>
</div>


<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
