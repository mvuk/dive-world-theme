<?php
/*
Template name: big deep pool
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="checkout">

			<!-- <?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?> -->

<!-- PAGE CONTENT STARTS HERE -->

<div class="row bigDeepIntro">
	<div class="large-12 column">
		<h1>Big Deep Pool</h1>
		<img src="/wp-content/uploads/2018/03/big-deep-pool-1200.jpg" alt="">
	</div>
</div>

<div class="row bigDeepCheckout">
	<div class="large-12 column">
		<div class="big-deep-disclaimer">
			* Currently this form supports one registrant at a time. For multiple diver signups, please submit the form then refresh your page.
		</div>
	</div>
	<div class="large-12 column">
		<div class="form">
			<?php echo do_shortcode('[gravityform id="24" title="false" description="true" ajax="true"]'); ?>
		</div>
	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
