<?php
/*
Template name: Blank Template
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row fullWidth">
  <div class="large-12 columns">
    
  </div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
