<?php
/*
Template name: Calendar
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="calendar-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.print.css" media='print'>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/gcal.js"></script>

<div class="row calendar">
	<div class="large-12 column">
		<div class="calendarTitle">
			<h1>Dive World Calendar</h1>
			<p>Stay up to date with the dive shop for course availability and upcoming community events.</p>
		</div>
	</div>
	<div class="large-12 column">
		<!-- START CALENDAR -->

		<script type='text/javascript'>

		$(document).ready(function() {
		    $('#calendar').fullCalendar({

					header: {
						left: 'prev,next',
						center: 'title',
						right: 'month,listYear'
					},

	        googleCalendarApiKey: 'AIzaSyAHNTgNRzRwKK0J9Q65i9MJoYu_O2Ms8wA',
	        events: {
	            googleCalendarId: 'dkhil9ei603lqiil5c6u7eh1l8@group.calendar.google.com'
	        },

					eventClick: function(event) {
	// opens events in a popup window
	window.open(event.url, 'gcalevent', 'width=700,height=600');
	return false;
}
		    });
		});

		</script>

		<div id='calendar'></div>

		<!-- END CALENDAR -->
	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
