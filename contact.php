<?php
/*
Template name: Contact
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row fullWidth slider">
	<div class="about-slider contact">

		<!-- <div class="nav-container">
			<div class="navigation">
				<div class="item">
					<a href="/about-us/about-dive-world/">About Dive World</a>
				</div>
				<div class="item">
					<a href="/about-us/meet-the-owner/">Meet the Owner</a>
				</div>
				<div class="item">
					<a href="/partners/">Partners</a>
				</div>
				<div class="item-active">
					<a href="/contact-us/">Contact Us</a>
				</div>
			</div>
		</div> -->

	</div>
</div>

<div class="row about-content">
	<div class="large-12 column">

		<div class="row title-row">
			<div class="large-12 column">
				<h1>Contact Us</h1>
			</div>
		</div>

		<div class="row content-row contact-row contact-three">
			<div class="large-4 column">
				<h2><i class="fa fa-phone" aria-hidden="true"></i>Call Us</h2>
				<p><a class="link phone" href="tel:+14165033483">416-503-DIVE</a></p>
				<p class="dive-phone">(416-503-3483)</p>
			</div>
			<div class="large-4 column">
				<h2><i class="fa fa-envelope-o" aria-hidden="true"></i>Email</h2>
				<p><a class="link email" href="mailto:info@diveworld.ca">info@diveworld.ca</a></p>
			</div>
			<div class="large-4 column">
				<h2><i class="fa fa-map-marker" aria-hidden="true"></i>Find Us</h2>
				<p class="address">3138 Lake Shore Blvd W<br>Toronto, ON M8V 1L4<br>Canada</p>
			</div>
		</div>

		<div class="row content-row">
			<div class="large-6 column">
				<img src="/wp-content/uploads/2018/05/diveworld_logo_v3.png" alt="Dive World">
			</div>
			<div class="large-6 column">
				<h2>Store Hours</h2>
				<table class="hours-table">
					<tbody>
						<tr>
						<td class="HoursBody">Monday</td>
						<td class="HoursBody">Closed</td>
						</tr>
						<tr>
						<td class="HoursBody">Tuesday</td>
						<td class="HoursBody">11:00am – 7:00pm</td>
						</tr>
						<tr>
						<td class="HoursBody">Wednesday</td>
						<td class="HoursBody">11:00am – 7:00pm</td>
						</tr>
						<tr>
						<td class="HoursBody">Thursday</td>
						<td class="HoursBody">11:00am – 7:00pm</td>
						</tr>
						<tr>
						<td class="HoursBody">Friday</td>
						<td class="HoursBody">11:00am – 7:00pm</td>
						</tr>
						<tr>
						<td class="HoursBody">Saturday</td>
						<td class="HoursBody">11:00am – 3:00pm</td>
						</tr>
						<tr>
						<td class="HoursBody">Sunday</td>
						<td class="HoursBody">11:00am – 3:00pm</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row content-row contact-row">
			<div class="large-6 column">
				<h2>Our Location</h2>
				<p>Dive World is located just east of Kipling and Lakeshore. Free parking is available in the lot right next door to our Dive Shop.</p>
				<img src="/wp-content/uploads/2015/02/dive-world-location-e1432749348991.png">
			</div>
			<div class="large-6 column">
				<h2>Our Store</h2>
				<p>A view from Lake Shore Blvd W. Dive World is located on the North side of the street.</p>
				<img src="/wp-content/uploads/2018/08/dive-world-storefront-august-2018.jpg">
			</div>
		</div>

	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
