<?php
/*
Template name: Countdown Landing Page 1
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>

	<?php the_excerpt(); ?>

<?php } ?>

<div id="content" role="main" class="clp-content">

			<?php while ( have_posts() ) : the_post(); ?>
			<?php endwhile; // end of the loop. ?>
	
  <!-- BEGIN LANDING PAGE CONTENT -->
  
  <!-- LANDING PAGE HEADER -->
  
  <div class="clp-header">
    <div class="clp-header-container">
    <div class="clp-logo"></div>
    <div class="clp-questions"><div class="clp-phone"><i class="fa fa-phone" aria-hidden="true"></i>Questions? Call us at 416-503-3483</div></div>
    </div>
  </div>
  
  <!-- LANDING PAGE BODY CONTENT -->
  
  <div class="clp-promo-intro">
    
    <div class="clp-video-promo">
    <div class="clp-video">
      
      <div class="ow-lp-video"><iframe src="https://player.vimeo.com/video/199689967?autoplay=1&color=ff6666&title=0&byline=0&portrait=0" width="800" height="450" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
      
    </div>
    
    <div class="clp-promo">
    
      <div class="clp-promo-box">
      
        <div class="clp-price-box">
          <div class="clp-promo-price">$<?php the_field('promo_price'); ?></div>
          <div class="clp-price-label-row">
            <div class="clp-price-label">Value</div>
            <div class="clp-price-label">Discount</div>
            <div class="clp-price-label">You Save</div>
          </div>
          <div class="clp-price-row">
            <div class="clp-prices">C$<?php the_field('promo_value'); ?></div>
            <div class="clp-prices"><?php the_field('promo_discount_percent'); ?>%</div>
            <div class="clp-prices">C$<?php the_field('you_save'); ?></div>
          </div>
        </div>
        
        <div class="clp-promo-active <?php the_field('promotion_running'); ?>">
        <div class="buy-btn-dsd"><a class="buy-dsd" href="#buy-sc-course" rel="m_PageScroll2id">Buy Now</a></div>
        <div class="clp-limited-time"><i class="fa fa-clock-o sc-clock"></i>Limited Time Remaining!</div>
        <div class="clp-countdown"><?php the_field('promotion_end_date'); ?></div>
        </div>
        
        <div class="clp-promo-inactive <?php the_field('promotion_running'); ?>">
          <div class="clp-promo-expired"><i class="fa fa-clock-o sc-clock"></i>Promotion Expired!</div>
        </div>
        
      </div>
      
    </div>
    </div>
  </div>
    <!-- course description area -->
    
  <div class="clp-description"><div class="clp-desc-2">

    <div class="clp-three-components">

      <div class="clp-three-title"><h2>In order to go diving you will need to get a diving license.<br>Earning your license involves three steps:</h2></div>  
      <div class="clp-three-div">

        <div class="clp-three-item clp-theory">
          <div class="clp-three-img"></div>
          <div class="clp-three-content">
            <h3>Theory</h3>
            <p>This can be done from the comfort of your own home</p>
          </div>
        </div>

        <div class="clp-three-item clp-pool">
          <div class="clp-three-img"></div>
          <div class="clp-three-content">
            <h3>Pool</h3>
            <p>Two days of pool training on the weekend, all year-round</p>
          </div>
        </div>

        <div class="clp-three-item clp-dives">
          <div class="clp-three-img"></div>
          <div class="clp-three-content">
            <h3>Open Water</h3>
            <p>Two days of open water dives<br>(between June and October)</p>
          </div>
        </div>

      </div>

    </div>
    </div>

    <div class="clp-included-desc">
      
      <div class="clp-three-options">
      
        <h2>We have three options to meet your needs</h2>
        
        <div class="clp-three-div">
        	
          <div class="clp-three-course clp-basic">
            <div class="clp-three-img-c"></div>
            <div class="clp-three-desc">
              <h3>Basic Course</h3>
              <h4>$229</h4>
              <p class="tooltips tooltip-p" title="Ideal for individuals who are comfortable in the water and can keep up with a group pace">10-1 Student to Instructor ratios</p>
              <p>All course materials</p>
              <p class="tooltips tooltip-p" title="For 90% of people this is enough to get through all of the pool training">2 days of pool training</p>
              <p class="tooltips tooltip-p" title="Mask, Snorkel and Fins not included">All Scuba Rentals</p>
              <p>Referral Paperwork</p>
            </div>
          </div>
          
          <div class="clp-three-course clp-standard">
                      <div class="clp-three-img-c"></div>
            <div class="clp-three-desc">
              <h3>Standard Course</h3>
              <h4>$349</h4>
              <p class="tooltips tooltip-p" title="Smaller ratios allow for more personalized attention">5-1 Student to Instructor ratios</p>
              <p>All course materials</p>
              <p class="tooltips tooltip-p" title="If you require additional time and instruction we continue to work with you at no additional charge">Unlimited in-pool instruction</p>
              <p class="tooltips tooltip-p" title="Mask, Snorkel and Fins not included">All Scuba Rentals</p>
              <p>Referral Paperwork</p>
            </div>
          </div>
          
          <div class="clp-three-course clp-private">
                      <div class="clp-three-img-c"></div>
            <div class="clp-three-desc">
              <h3>Private Course</h3>
              <h4>$699</h4>
              <p class="tooltips tooltip-p" title="Custom personalized training allows us to teach at your pace">1 on 1 Instruction</p>
              <p>All course materials</p>
              <p class="tooltips tooltip-p" title="If you require additional time and instruction we continue to work with you at no additional charge">Unlimited in-pool instruction</p>
              <p class="tooltips tooltip-p" title="Mask, Snorkel and Fins not included">All Scuba Rentals</p>
              <p>Referral Paperwork</p>
            </div>
          </div>
          
        </div>
        
      </div>
      
      <div class="clp-included-copy">
        <h2>Our courses also include:</h2>
        <ul>
          <p class="tooltips tooltip-p" title="Refresh your skills with an instructor free of charge."><i class="fa fa-check-square-o" aria-hidden="true"></i>Free Scuba Tune Ups for Life</p>
 					<p class="tooltips tooltip-p" title="Scuba Equipment Rental not included"><i class="fa fa-check-square-o" aria-hidden="true"></i>1 Year Free Membership to our pool</p>
					<p class="tooltips tooltip-p" title="$50 towards Mask, Snorkel, Fins & Boots PLUS $50 towards BCD, Regulator or Dive Computer"><i class="fa fa-check-square-o" aria-hidden="true"></i>$100 in gift certificates</p>
        </ul>
      </div>
    
    </div>
    
    <div class="clp-reviews">
      <div class="clp-reviews-line">
    	  <h2>100% of reviewers reccomend Dive World</h2>
      </div>
      <div class="clp-reviews-img"></div>
      <div class="clp-link">
    	  <a href="/reviews" target="_blank">Read Customer Reviews</a>
      </div>
    </div>
    
      <div class="clp-ticket-section" id="buy-sc-course">
    
      <div class="slp-ticket-selector">
      
        <h2>We cherish the privilege of being the ones to introduce you to this incredible sport.</h2>
        <p>Register below or give us a call and we can help you choose the option best suited to your needs.</p>
        
        <div class="clp-ticket-selector-div">
<?php
	$clpticket = get_field( "event_id" ); 

	
    echo do_shortcode($clpticket);
?>
        </div>
        <div class="clp-ssl-area">
          <div class="clp-ssl"><div class="clp-ssl-img"></div></div>
          <div class="clp-secure-payment"><div class="clp-secure-img"></div></div>
        </div>
        </div>
      </div>
      
    </div>
  <div class="clp-outro <?php the_field('promotion_running'); ?>">
    <div class="clp-question-call-us"><p>Any Questions? Call us at <span>416-503-3483</span></p></div>
  </div>
  </div>

  
</div>
<?php get_footer(); ?>
