<?php
/*
Template name: Emergency Oxygen Provider Specialty
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div  class="page-wrapper page-left-sidebar">
<div class="row">

<div id="content" class="large-9 right columns course-template" role="main">
	<div class="page-inner">
			<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template();
					?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="large-12 column">
	<img src="/wp-content/uploads/2018/08/dive-world-oxygen-course.jpg" alt="emergency oxygen provider" class="course-intro-img">
</div>

<div class="large-12 column">
	<!-- <div class="large-12 columns video-intro">
		<iframe src="https://www.youtube.com/embed/tWPGi52nkHg" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
	</div> -->
	<p>Knowing how and when to use emergency oxygen is a great skill to have and means you’re ready to help others should the need arise. Becoming a PADI Emergency Oxygen Provider lets you breathe easy knowing that you can recognize scuba diving illnesses treatable with emergency oxygen, and are prepared to offer aid.</p>
	<p>There are no prerequisites, age restrictions or water sessions required for this course – it’s open to everyone. Scuba divers, snorkelers and anyone who is around divers – boat crew, lifeguards, etc. – will benefit from having this training.</p>
</div>

<div class="large-12 column">
	<p>Learn how to:</p>
	<ul>
		<li>Assemble and disassemble emergency oxygen equipment.</li>
		<li>Deploy a non-rebreather mask and a demand inhalator valve on a breathing diver.</li>
		<li>Use a pocket mask on a nonbreathing diver.</li>
	</ul>
	<p>We will also teach you about dive injuries, the different types of oxygen equipment you can encounter and safety considerations what using oxygen.</p>
</div>

<div class="large-12 column course-register">
	<p>Course Costs: <strong>$185</strong></p>
	<p>Included:</p>
	<ul>
		<li>Emergency Oxygen Provider Specialty Manual</li>
		<li>License Processing Fee</li>
	</ul>
	<p>This certification counts as 1 out of the 5 specialties required to earn the Master Scuba Diver accreditation.</p>
</div>

<!-- START CHECKOUT ELEMENT -->
<div class="large-12 columns aow-final">
	<h2>Register for PADI Emergency Oxygen Provider</h2>
	<p>Register below and we will walk you through what's next.</p>
	<div class="large-12 columns ticket-selector-div">
		<div class="ticket-selection">
				<div class="pseudoCourseRegDiv">
				<a href="https://diveworld.ca/courses/emergency-oxygen-provider/emergency-oxygen-provider-specialty/" class="a-pseudoCourseRegDiv">Register Now</a>
				</div>
		</div>
		<div class="large-6 columns ssl">
			<div class="large-12 columns">
				<div class="large-4 small-4 columns">
					<div class="ssl-img"></div>
				</div>
				<div class="large-8 small-8 columns">
					<p>Your payment to Dive World Inc. is secured with an SSL Certificate by GeoTrust</p>
				</div>
			</div>
		</div>
		<div class="large-6 columns secure-payment">
			<div class="large-12 columns">
				<p>Secure Payment Options <i class="fa fa-lock" aria-hidden="true"></i></p>
				<div class="secure-payment-img"></div>
			</div>
		</div>
	</div>
</div>
<!-- END CHECKOUT ELEMENT -->

<!-- PAGE CONTENT ENDS HERE -->

	</div><!-- .page-inner -->
</div><!-- end #content large-9 left -->

<div class="large-3 columns left">
<?php get_sidebar(); ?>
</div><!-- end sidebar -->

</div><!-- end row -->
</div><!-- end page-right-sidebar container -->


<?php get_footer(); ?>
