<?php
/*
Template name: Equipment Specialty Course
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div  class="page-wrapper page-left-sidebar">
<div class="row">

<div id="content" class="large-9 right columns course-template" role="main">
	<div class="page-inner">
			<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template();
					?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="large-12 column">
	<img src="/wp-content/uploads/2017/03/contact-table.jpg" alt="Equipment Specialty Course" style="margin-bottom:25px;">
</div>

<div class="large-12 column">
	<!-- <div class="large-12 columns video-intro">
		<iframe src="https://www.youtube.com/embed/tWPGi52nkHg" width="640" height="360" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
	</div> -->
	<p>One of our most popular and rewarding programs.</p>
	<p>In this program we teach divers the ins and outs of their scuba gear. You will walk away with a deeper understanding and respect for your scuba gear.</p>
</div>

<div class="large-12 column">
	<strong><p>Learn how to:</p></strong>
	<ul>
		<li>Diagnose and service 90% of regular related issues</li>
		<li>Learn how regulators work, the differences and benefits of unique models</li>
		<li>Service BCD's</li>
		<li>Service tank valves</li>
		<li>We also discuss but spend less time on: wetsuit materials and benefits, mask designs, snorkel models and features, fin designs & recommended equipment configurations</li>
	</ul>
	<p>This is a great opportunity to bring in your personal gear and learn how to perform routine maintenance and minor service on your own gear.</p>
</div>

<div class="large-12 column course-register">
	<p>Course Costs: <strong>$254</strong></p>
	<strong><p>Included:</p></strong>
	<ul>
		<li>PADI Equipment Specialty Manual</li>
		<li>Dive Master Tool Kit</li>
		<li>License processing fee</li>
	</ul>
	<p>This certification counts as 1 out of the 5 specialties required to earn the Master Scuba Diver accreditation.</p>
</div>

<!-- START CHECKOUT ELEMENT -->
<div class="large-12 columns aow-final">
	<h2>Register for Equipment Specialty Course</h2>
	<p>Register below and we will walk you through what's next.</p>
	<div class="large-12 columns ticket-selector-div">
		<div class="ticket-selection">
				<div class="pseudoCourseRegDiv">
				<a href="/courses/equipment-specialty-course/equipment-specialty-course-checkout" class="a-pseudoCourseRegDiv">Register Now</a>
				</div>
		</div>
		<div class="large-6 columns ssl">
			<div class="large-12 columns">
				<div class="large-4 small-4 columns">
					<div class="ssl-img"></div>
				</div>
				<div class="large-8 small-8 columns">
					<p>Your payment to Dive World Inc. is secured with an SSL Certificate by GeoTrust</p>
				</div>
			</div>
		</div>
		<div class="large-6 columns secure-payment">
			<div class="large-12 columns">
				<p>Secure Payment Options <i class="fa fa-lock" aria-hidden="true"></i></p>
				<div class="secure-payment-img"></div>
			</div>
		</div>
	</div>
</div>
<!-- END CHECKOUT ELEMENT -->

<!-- PAGE CONTENT ENDS HERE -->

	</div><!-- .page-inner -->
</div><!-- end #content large-9 left -->

<div class="large-3 columns left">
<?php get_sidebar(); ?>
</div><!-- end sidebar -->

</div><!-- end row -->
</div><!-- end page-right-sidebar container -->


<?php get_footer(); ?>
