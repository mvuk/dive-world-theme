<?php
/* ADD custom theme functions here  */

/*** Remove Query String from Static Resources ***/
function remove_cssjs_ver( $src ) {
 if( strpos( $src, '?ver=' ) )
 $src = remove_query_arg( 'ver', $src );
 return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

/**
 * woocommerce_package_rates is a 2.1+ hook
 */
add_filter( 'woocommerce_package_rates', 'hide_shipping_when_free_is_available', 10, 2 );
 
/**
 * Hide shipping rates when free shipping is available
 *
 * @param array $rates Array of rates found for the package
 * @param array $package The package array/object being shipped
 * @return array of modified rates
 */
function hide_shipping_when_free_is_available( $rates, $package ) {
 	
 	// Only modify rates if free_shipping is present
  	if ( isset( $rates['free_shipping'] ) ) {
  	
  		// To unset a single rate/method, do the following. This example unsets flat_rate shipping
  		unset( $rates['flat_rate'] );
  		
  		// To unset all methods except for free_shipping, do the following
  		$free_shipping          = $rates['free_shipping'];
  		$rates                  = array();
  		$rates['free_shipping'] = $free_shipping;
	}
	
	return $rates;
}

/* SET BRANDS IN GOOGLE MERCHANT CENTER */

function woocommerce_gpf_add_brand( $elements, $product_id ) {
    $brands = wp_get_object_terms( $product_id, 'product_brand', array( 'fields'=>'names' ) );
    if ( ! empty( $brands ) ) {
        $elements['brand'] = $brands;
    }
    return $elements;
}
add_filter( 'woocommerce_gpf_elements', 'woocommerce_gpf_add_brand', 11, 2 );


/** 
 * Changes the URL used for the Google Sitelink Search Box functionality in WordPress SEO (Premium) 
 * The returned string must always include {search_term} to indicate where the search term should be used.
 * 
 * @returns string new searchURL
 */
 function  yoast_change_ssb_search() {
  return 'https://diveworld.ca/?s={search_term}&post_type=product';
 }
 
add_filter('wpseo_json_ld_search_url', 'yoast_change_ssb_search' ); 