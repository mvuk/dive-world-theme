<?php
/*
Template name: grand cayman
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row tripRow top">
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2018/02/cayman-image-1.jpg" alt="">
	</div>
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2018/02/cayman-image-2.jpg" alt="">
	</div>
</div>

<div class="row tripIntro">
	<div class="large-12 column">
    <h1>GRAND CAYMAN</h1>
		<p class="intro-dates">February 2-9, 2019</p>
		<p class="trip-desc">With a variety of rooms, a quiet location on a rocky shoreline renowned for its shore diving, and only a 15-minute walk from downtown, the highly recommended Sunset House is sure to please even the most avid diver.</p>
		<p class="trip-desc">While we do not have a beach, the pool with a waterfall, whirlpool feature and sea pool with direct ladder access to the Caribbean provides all of the water based activities most snorkelers and divers could ask for. Tired of the water? Visit our praised and local-frequented Sea Harvest Restaurant and My Bar.</p>
		<a href="https://dw352.infusionsoft.com/app/orderForms/Dive-World-Sunset-House-Grand-Cayman-Feb-2019" class="registerOnline">Register Online</a>
	</div>
</div>

<div class="row tripRow">
	<div class="large-12 column">
		<img src="/wp-content/uploads/2018/02/grand-cayman-sunset-house-2-9-feb-2019.jpg" alt="">
	</div>
</div>


<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
