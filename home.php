<?php
/*
Template name: Home
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row fullWidth tiles intro">
  <div class="large-8 columns">
		<div class="h-tile">
			<p>learn to dive in toronto</p>
		</div>
  </div>
	<div class="large-4 columns">
		<div class="h-tile">
			<p>cool featured tile</p>
		</div>
	</div>
</div>

<div class="row fullWidth tiles">
	<div class="large-4 columns">
		<div class="h-tile">
			<p>tile</p>
		</div>
	</div>
	<div class="large-4 columns">
		<div class="h-tile">
			<p>tile</p>
		</div>
	</div>
	<div class="large-4 columns">
		<div class="h-tile">
			<p>tile</p>
		</div>
	</div>
</div>

<div class="row fullWidth tiles">
	<div class="large-4 columns">
		<div class="h-tile">
			<p>tile</p>
		</div>
	</div>
	<div class="large-4 columns">
		<div class="h-tile">
			<p>tile</p>
		</div>
	</div>
	<div class="large-4 columns">
		<div class="h-tile">
			<p>tile</p>
		</div>
	</div>
</div>

<div class="row fullWidth tiles">
	<div class="large-5 columns">
		<div class="h-tile">
			<p>map location</p>
		</div>
	</div>
	<div class="large-7 columns">
		<div class="h-tile">
			<p>contact info</p>
		</div>
	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
