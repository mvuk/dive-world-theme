<?php
/*
Template name: Learn
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row learnpage">
	<div class="large-12 column">
		<!--  -->


		<ul>
		<!-- // Define our WP Query Parameters -->
		<?php $the_query = new WP_Query( 'posts_per_page=20' ); ?>

		<!-- // Start our WP Query -->
		<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

		<div class="postLearn">
			<!-- // Display the Post Title with Hyperlink -->
			<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
			<!-- // Display the Post Excerpt -->
			<p><?php the_excerpt(__('(more…)')); ?></p>
		</div>

		<!-- // Repeat the process and reset once it hits the limit -->
		<?php
		endwhile;
		wp_reset_postdata();
		?>
		</ul>

		<!--  -->
	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
