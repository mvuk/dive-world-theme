<?php
/*
Template name: Marine Research Assistant Program
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="marine-research-assistant-program">

<div class="row">
	<div class="large-12 column">
		<img src="/wp-content/uploads/2019/01/Research-Assistant3-1.jpg" class="mrap-slider-img" alt="">
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<h1>Marine Research Assistant Program</h1>
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<h2>About the Research Assistant Program</h2>
		<p>The Institute for Marine Research in Dauin, Philippines offers opportunities for divers of all experience levels. Their Research Assistants (RAs) experience a unique opportunity to assist our Science Staff in the attainment and processing of all data collected from the Dauin Long-Term Reef Monitoring Project. This one-month course is intended as an introduction into marine conservation science.</p>
		<p>Research Assistants will not only learn practical, theoretical, problem-solving and analytical skills that will enhance their prospects as future conservationists, but will learn how to communicate science to local stakeholders through school education and community outreach programs.</p>
		<p>The Institute for Marine Research & The Dauin Municipality Long-Term Reef Monitoring Project aim to track fine-scale changes in the overall reef community over time. They are looking to better understand how benthic composition influence fish community structure and invertebrate community composition. They also aim to document the effect of pH, currents, and disturbances, both natural and anthropogenic.</p>
	</div>
</div>

<div class="row">
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2019/01/Dauin-Inshore-Reef-1.jpg" alt="" class="half-image">
	</div>
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2019/01/Apo-Island-1.jpg" alt="" class="half-image">
	</div>	
</div>

<div class="row">
	<div class="large-12 column">
		<h2>What's included?</h2>
		<ul>
			<li>Airport or Ferry Pick-up from Dumaguete</li>
			<li>Accommodation</li>
			<li>3 meals Monday – Saturday (Inlcuding Water, Tea and Coffee on site).</li>
			<li>Internet Access</li>
			<li>In-country Support</li>
			<li>All training and survey dives (~50 per month)</li>										
			<li>11x Coral Reef Ecology and Species Identification Lectures conducted by experienced Marine Scientists</li>
			<li>IMR T-Shirt</li>
			<li>Welcome and Good-Bye Dinner</li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<h2>Our Research Techniques</h2>
		<p>Obtaining High Quality Data for Conservation Management</p>
	</div>
</div>

<div class="row">
	<div class="large-4 columns">
		<img src="/wp-content/uploads/2019/01/MG_3455-1.jpeg" alt="" class="mrap-third-image">
	</div>
	<div class="large-4 columns">
		<img src="/wp-content/uploads/2019/01/MG_3464-1.jpeg" alt="" class="mrap-third-image">
	</div>
	<div class="large-4 columns">
		<img src="/wp-content/uploads/2019/01/MG_3487-1.jpeg" alt="" class="mrap-third-image">
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<h3>Diver Operated Stereo Video System</h3>
		<p>The Institute for Marine Research uses Diver Operated Stereo Video Systems for fish density, fish 3D distribution mapping and fish size measurements. This innovative technology allows our Researchers to not only record fish species with both precision and accuracy than the traditional Underwater Visual Census (UVC) techniques, but efficiently quantifying the abundance and size of reef fish.</p>
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<h2>3-Dimensional (3D) Reef Modelling</h2>
		<p>On tropical coral reefs, structural complexity has been shown to be a major driver of productivity, biodiversity, and overall functionality of reef ecosystems. To understand more precisely the nature between structural complexity and reef health, we use 3-Dimensional (3D) Modelling as a tool to quantify topography, rugosity and other structural characteristics that play an important role in the ecology of coral reef communities. This 3D reconstruction of reef structure and complexity becomes integrated with other physiological and ecological parameters to improve monitoring of the health and function of coral reef ecosystems in Philippines, whilst providing a visual view of complexity change to IMRs partnering coastal managers.</p>
		<img src="/wp-content/uploads/2019/01/3-Dimensional-3D-Reef-Modelling.jpg" alt="">
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<h2>Impacts & Ocean Monitoring Sensors</h2>
		<p>The Institute for Marine Research deploys data loggers, and conducts impact surveys to provide a more detailed picture of the causes and relative scale of coral mortality and ecosystem change. The Institute for Marine Research uses fine-scale research methodology to assess the following impacts:</p>
	</div>
</div>

<div class="row">
	<div class="large-6 columns">
		<div class="mrap-image-label">
			<h3>Coral Bleaching</h3>
			<img src="http://diveworld.ca/wp-content/uploads/2019/01/Impacts-Ocean-Monitoring-Sensors.jpg" alt="" class="half-image">		
		</div>
	</div>
	<div class="large-6 columns">
		<div class="mrap-image-label">
			<h3>Coral Disease</h3>
			<img src="http://diveworld.ca/wp-content/uploads/2019/01/Coral-Disease.jpg" alt="" class="half-image">		
		</div>
	</div>
</div>
<div class="row">
	<div class="large-6 columns">
		<div class="mrap-image-label">
			<h3>Corallivorous Invertebrates</h3>
			<img src="http://diveworld.ca/wp-content/uploads/2019/01/Corallivorous-Invertebrates.jpg" alt="" class="half-image">		
		</div>
	</div>
	<div class="large-6 columns">
		<div class="mrap-image-label">
			<h3>Pollution</h3>
			<img src="http://diveworld.ca/wp-content/uploads/2019/01/Coral-Pollution.jpg" alt="" class="half-image">		
		</div>
	</div>
</div>

</div>


<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
