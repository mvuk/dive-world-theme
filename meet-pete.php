<?php
/*
Template name: Meet pete
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row fullWidth slider">
	<div class="about-slider meet-the-pete">

		<div class="nav-container">
			<div class="navigation">
				<div class="item">
					<a href="/about-us/about-dive-world/">About Dive World</a>
				</div>
				<div class="item">
					<a href="/about-us/meet-the-owner/">Meet the Owner</a>
				</div>
				<div class="item-active">
					<a href="/about-us/meet-training-director/">Meet the Training Director</a>
				</div>
				<div class="item">
					<a href="/partners/">Partners</a>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="row about-content">
	<div class="large-9 column">

		<div class="row title-row">
			<div class="large-12 column show-for-small mobile-pic">
				<!-- <img class="mario-meet-owner" src="/wp-content/uploads/2018/04/pete-gilbert-dive-world-canada.jpg" alt="Pete Gilbert"> -->
			</div>
			<div class="large-12 column">
				<h1>Meet the Training Director</h1>
			</div>
		</div>

		<div class="row content-row">
			<div class="large-12 column">
				<h2>Pete Gilbert</h2>
				<h3>Director of Training</h3>
				<p>Pete Gilbert was born to dive!</p>
				<p>He started working in the family dive business when he was in his early teens and was already running the business by the time he graduated highschool!</p>
				<p>Pete turned pro in 2009 and has spent a large portion of his career working as the General Manager of one of the largest dive shops in the Caribbean.</p>
				<p>While there he specialized in developing new programs and facilitating job placement opportunities for his students. He was also the primary operator for a Hyperbaric Chamber and developed a passion for understanding the physics and patho-physiology of diving.</p>
				<p>Pete is a phenomenal instructor, a vastly experienced diver and overall scuba ninja. He holds the distinction of PADI Staff Instructor, Dive-Heart Adaptive Scuba Instructor Trainer, ANDI Hyperbaric Chamber Operator Instructor, as well as over a dozen Specialty Instructor ratings.</p>
			</div>
		</div>

	</div>
	<div class="large-3 column show-for-medium-up">
		<!-- <img class="mario-meet-owner" src="/wp-content/uploads/2018/04/pete-gilbert-dive-world-canada.jpg" alt="Pete Gilbert"> -->
	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
