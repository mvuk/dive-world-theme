<?php
/*
Template name: Meet the Owner
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row fullWidth slider">
	<div class="about-slider meet-the-owner">

		<div class="nav-container">
			<div class="navigation">
				<div class="item">
					<a href="/about-us/about-dive-world/">About Dive World</a>
				</div>
				<div class="item-active">
					<a href="/about-us/meet-the-owner/">Meet the Owner</a>
				</div>
				<div class="item">
					<a href="/about-us/meet-training-director/">Meet the Training Director</a>
				</div>
				<div class="item">
					<a href="/partners/">Partners</a>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="row about-content">
	<div class="large-9 column">

		<div class="row title-row">
			<div class="large-12 column show-for-small mobile-pic">
				<!-- <img class="mario-meet-owner" src="/wp-content/uploads/2017/03/mario-medarevic-owner-of-dive-world.png" alt="Mario Medarevic"> -->
			</div>
			<div class="large-12 column">
				<h1>Meet the Owner</h1>
			</div>
		</div>

		<div class="row content-row">
			<div class="large-12 column">
				<h2>Mario Medarevic</h2>
				<h3>Dive World Founder</h3>
				<p>Mario Medarevic is a Yugoslav born water fanatic that grew up in Malaysia. Like so many other young boys, Mario was obsessed with sharks as a child spending every spare moment between school and sleep meticulously studying his books on sharks and fantasizing about the day he would be able to interact with these majestic creatures.</p>
				<p>Mario spent the better part of his childhood training as a competitive swimmer and managed to make it all the way to the national level here in Canada at the age of 19. It was his proficiency as a swimmer, and his obsession with sharks that finally lead him to pursue a career in diving after graduating from university. He began his diving career in Thailand where he accumulated thousands of dives over four years learning from seasoned instructors, and experts in marine biology. He later went on to live on the remote islands of northern Sumatra where the relatively unspoiled marine ecosystems offered the perfect opportunity to quench his thirst for underwater exploration and interaction with large marine predators. Mario has since dove around the world and accumulated firsthand experience with a variety of marine ecosystems, animals and people from all walks of life and in the process has gained a true appreciation for the extent of marine degradation and animal plight.</p>
				<p>Mario is an extraordinary, very experienced diver and overall super nice guy! He has also worked more than a decade as a professional SCUBA (PADI MSDT 10+ Year,) Freedive instructor (Constant Weight Depth: 175ft Static Breath Hold: 6:30 min,) and professional safety diver for productions and research programs around the world. He is also an instructor for 1st Aid/CPR/E02/AED/WFR/EMT/DEMP as well as adviser, trainer, and designer of accident management systems for major corporations with dive programs.</p>
				<p>Mario’s real passion is Sharks which is why if he is not in Toronto, you can trust he is somewhere diving with these majestic creatures.</p>
				<p>Mario founded Dive World in his now home city of Toronto with the mission of bridging the gap between city dwellers and the marine ecosystems that desperately need their help.</p>
				<p>You can always visit Dive World and speak to Mario directly about what you can do to make a difference in marine conservation.</p>
			</div>
		</div>

	</div>
	<div class="large-3 column show-for-medium-up">
		<!-- <img class="mario-meet-owner" src="/wp-content/uploads/2017/03/mario-medarevic-owner-of-dive-world.png" alt="Mario Medarevic"> -->
	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
