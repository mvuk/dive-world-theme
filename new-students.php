<?php
/*
Template name: New students
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="reduced-header">
	<img src="/wp-content/uploads/2015/06/dive-world-head-logo-6.png" alt="" class="reduced-logo">
	<div class="reduced-number">
		(416) 503-3483
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<div class="newstudents">
			<h1 class="weekend">Weekend Training Schedule</h1>
			<div class="schTime"><span class="clock"><i class="fa fa-clock-o"></i></span> 11:30am - 12pm : Students Arrive</div>
			<p class="schP">This is the ideal time to get fitted for your own set of personal gear by your instructor. If you prefer to rent we do have that option available as well.</p>
		</div>
	</div>
</div>

<div class="not-suitable-gear">
	<div class="row">
		<div class="large-4 columns">
			<img src="/wp-content/uploads/2018/05/91XLf3-C4gL._SX466_.jpg" alt="">
		</div>
		<div class="large-8 columns">
			<h2>Although tolerable for pool training, this type of personal gear is not suitable for open water scuba diving.</h2>
			<p>When you factor in the increased water resistance of scuba gear, waves and currents, short fins will not provide you with enough propulsion to dive safely. Scuba masks also need to have tempered glass (not plastic) lenses to ensure they are pressure rated for depth.</p>
		</div>
	</div>
</div>


<div class="row new-bundles">
	<div class="large-12 column">
		<h2 class="sets">Recommended Starting Sets</h2>
	</div>
	<div class="large-4 columns">
		<div class="bundle">
			<h3 class="bundleTitle">Caribbean Package</h3>
			<img src="/wp-content/uploads/2018/05/caribbean-bundle.png" alt="">
			<div class="bundleDesc">
				<ul>
				<li>Choice of mask (max $75 value)</li>
				<li>Semi-dry snorkel</li>
				<li>Low cut boots</li>
				<li>Travel fins</li>
				<!-- <li>Rash guard for comfort and UV protection</li> -->
				<li>Suitable for warm water diving</li>
				</ul>
			</div>
			<div class="bundlePrice">
				<h4 class="fullprice">Regular Price: <span class="fullprice">$255.95</span></h4>
				<h4 class="price">Student Price: <span class="number">$205.95</span></h4>
			</div>
		</div>
	</div>
	<div class="large-4 columns">
	<div class="bundle">
			<h3 class="bundleTitle">North & South Combo</h3>
			<img src="/wp-content/uploads/2018/05/north-south-combo.jpg" alt="">
			<div class="bundleDesc">
				<ul>
				<li>Choice of premium masks <span>(max $110 value)</span></li>
				<li>Semi Dry Snorkel</li>
				<li>7mm Titanium High top boots</li>
				<li>Coronado versatile starter fin</li>
				<!-- <li>Rash guard for comfort and UV protection</li> -->
				<li>Suitable for all diving conditions</li>
				</ul>
			</div>
			<div class="bundlePrice">
				<h4 class="fullprice">Regular Price: <span class="fullprice">$359.95</span></h4>
				<h4 class="price">Student Price: <span class="number">$309.95</span></h4>
			</div>
	</div>
	</div>
	<div class="large-4 columns">
	<div class="bundle">
			<h3 class="bundleTitle">Explorer Combo</h3>
			<img src="/wp-content/uploads/2018/05/explorer-combo.jpg" alt="">
			<div class="bundleDesc">
				<ul>
				<li>Choice of premium masks <span>(max $110 value)</span></li>
				<li>Semi-dry Snorkel</li>
				<li>7mm Titanium High top boots</li>
				<li>Pro Blue Fins <span class="diveworldpick">Dive World Pick</span></li>
				<li>Spring Strap Upgrades</li>
				<!-- <li>Rash guard for comfort and UV protection</li> -->
				<li>Suitable for all diving conditions</li>
				</ul>
			</div>
			<div class="bundlePrice">
				<h4 class="fullprice">Regular Price: <span class="fullprice">$424.95</span></h4>
				<h4 class="price">Student Price: <span class="number">$374.95</span></h4>
			</div>

	</div>
	</div>

</div>

<div class="row">
	<div class="large-12 column">
		<div class="newstudents">
			<div class="schTime"><span class="clock"><i class="fa fa-clock-o"></i></span> 12pm - 2pm : Classroom Time!</div>
			<p class="schP">We discuss knowledge reviews.</p>
			<p class="schP">Day 1: Chapters 1, 2 & 3</p>
			<p class="schP">Day 2: Chapters 4 & 5</p>
		</div>
		<div class="middleImg">
			<img src="/wp-content/uploads/2018/03/new-students-classroom.jpg" alt="classroom img">
		</div>
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<div class="newstudents">
			<div class="schTime"><span class="clock"><i class="fa fa-clock-o"></i></span> 2pm - 3pm : Scuba gear orientation</div>
			<p class="schP">We learn how to assemble, test, and disassemble our equipment.</p>
		</div>
		<div class="middleImg">
			<img src="/wp-content/uploads/2018/03/dive-world-shop-indoor-demonstration.jpg" alt="classroom img">
		</div>
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<div class="newstudents">
			<div class="schTime"><span class="clock"><i class="fa fa-clock-o"></i></span> 3:30pm - 6pm : Training at Norseman Pool</div>
			<p class="schP">Pool training session.</p>
			<p class="schP">Learn the fundamental skills of scuba diving and build your confidence underwater.</p>
		</div>
		<div class="middleImg">
			<img src="/wp-content/uploads/2018/03/about-dive-school-mario-teaching.jpg" alt="pool img">
		</div>
		<div class="reminder">
		<p>Reminder: The pool temperature is 80F (26C) so no wetsuits are required. However if you get cold easily it’s a good idea to bring a wetsuit.</p>
		</div>
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<h2 class="sets">Recommended Wetsuits</h2>
	</div>
	<div class="large-4 columns">
		<div class="bundle">
			<h3 class="bundleTitle">Neosport Shorty</h3>
			<img src="/wp-content/uploads/2015/12/BLRS3W_2-510x510.jpg" alt="">
			<div class="bundleDesc">
				<ul>
				<li>Perfect for warm Caribbean waters</li>
				<li>Protects from scraps, cuts and sunburns</li>
				<li>Keeps your core warm</li>
				<li>Easy to put on and take off</li>
				</ul>
			</div>
			<div class="bundlePrice">
				<h4 class="price">Price: <span class="number">$124.95</span></h4>
			</div>
		</div>
	</div>
	<div class="large-4 columns">
	<div class="bundle">
			<h3 class="bundleTitle">NeoSport Wetsuit</h3>
			<img src="/wp-content/uploads/2015/07/BLR3JM.jpg" alt="">
			<div class="bundleDesc">
				<ul>
				<li>3/2mm neoprene for added flexibility</li>
				<li>Suitable for warm water use</li>
				<li>Thicker torso for core body warmth</li>
				<li>Thinner arms and legs for easier maneuvering</li>
				</ul>
			</div>
			<div class="bundlePrice">
				<h4 class="price">Price: <span class="number">$169.95</span></h4>
			</div>
	</div>
	</div>
	<div class="large-4 columns">
	<div class="bundle">
			<h3 class="bundleTitle">5mm Thermaxx</h3>
			<img src="/wp-content/uploads/2017/01/T805MB_TherMAXX_BZ_Fullsuit_34_LEFT_WEB-510x510.jpg" alt="">
			<div class="bundleDesc">
				<ul>
				<li>Ideal for people who get cold easily</li>
				<li>Suitable for widest range of temperatures</li>
				<li>Resilient neoprene for long life span</li>
				<li>10 year warranty on the zippers</li>
				</ul>
			</div>
			<div class="bundlePrice">
				<h4 class="price">Price: <span class="number">$399.95</span></h4>
			</div>
	</div>
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<div class="newstudents">
			<div class="schTime"><span class="clock"><i class="fa fa-clock-o"></i></span> 6:30pm : Drop off gear at Dive World</div>
			<p class="schP">Debrief and log your progress.</p>
		</div>
		<div class="somethingtothinkabout">
		<p><strong>Something to think about:</strong></p>
		<p>As you will learn dive computers are a standard piece of scuba equipment designed to keep you safe and drastically reduce the risk of decompression sickness.</p>
		<p>We strongly recommend investing in your own dive computer to track your every move underwater and maximize your safety.</p>
		</div>
	</div>
</div>

<div class="row dive-computers">
	<div class="large-12 column">
		<h2 class="sets">Here are the three Dive Computers we recommend</h2>
	</div>
	<div class="large-4 columns">
		<div class="bundle">
				<h3 class="bundleTitle">Deepblu Cosmiq+</h3>
				<img src="/wp-content/uploads/2019/03/cosmiq-plus-front-black-camo-crop.png" alt="">
				<div class="bundleDesc">
					<ul>
					<li>Rechareable</li>
					<li>Bluetooth</li>
					<li>Intuitive, easy to navigate</li>
					<li>High contrast LCD Screen</li>
					<li>Free dive mode</li>
					<li>Nitrox Mode</li>
					</ul>
				</div>
				<div class="bundlePrice">
					<h4 class="fullprice">Regular Price: <span class="fullprice">$550.00</span></h4>
					<h4 class="price">Student Price: <span class="number">$500.00</span></h4>
				</div>
		</div>
	</div>
	<div class="large-4 columns">
		<div class="bundle">
				<h3 class="bundleTitle">Shearwater Perdix AI</h3>
				<img src="/wp-content/uploads/2016/01/persp_2.png" alt="">
				<div class="bundleDesc">
					<ul>
					<li>AA batteries</li>
					<li>Built in Compass</li>
					<li>Large, clear LCD dispay</li>
					<li>Intuitive, easy to navigate</li>
					<li>Bluetooth</li>
					<li>Made in Canada</li>
					</ul>
				</div>
				<div class="bundlePrice">
					<h4 class="fullprice">Regular Price: <span class="fullprice">$1200.00</span></h4>
					<h4 class="price">Student Price: <span class="number">$1150.00</span></h4>
				</div>
		</div>
	</div>
	<div class="large-4 columns">
		<div class="bundle">
				<h3 class="bundleTitle">Shearwater Teric</h3>
				<img src="/wp-content/uploads/2019/03/teric-Product-Features-3.png" alt="">
				<div class="bundleDesc">
					<ul>
					<li>Wireless recharging</li>
					<li>Built in Compass</li>
					<li>Ultra crisp customizable LCD dispay</li>
					<li>Intuitive, easy to navigate</li>
					<li>Bluetooth</li>
					<li>Wireless Air Integration</li>
					<li>Made in Canada</li>
					</ul>
				</div>
				<div class="bundlePrice">
					<h4 class="fullprice">Regular Price: <span class="fullprice">$1400.00</span></h4>
					<h4 class="price">Student Price: <span class="number">$1350.00</span></h4>
				</div>
		</div>
	</div>

</div>

<div class="row">
	<div class="large-12 column">
		<div class="nodoubt">
			<h2>There is no doubt that you will invest in this equipment at some point. Right now is the best time to do it.</h2>
			<p>This is the ideal opportunity to get the right equipment under the guidance of your instructors. </p>
			<p>Do not hesitate to ask questions we are here to help you make the right choices based on your needs.</p>
		</div>
		<div class="nodoubtimg">
			<img src="/wp-content/uploads/2018/03/full-snorkeling-gear.jpg" alt="">
		</div>
	</div>
</div>


<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
