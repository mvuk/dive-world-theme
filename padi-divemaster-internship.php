<?php
/*
Template name: padi divemaster internship
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="divemaster lp-intro-banner">

	<div class="content">
		<h1>PADI Divemaster Internship</h1>
		<h2>It’s more than a course,<br>it’s an identity</h2>
		<ul>
			<li>Become a professional</li>
			<li>Work anywhere in the world</li>
		</ul>
		<a href="#needToKnow" class="start" rel="m_PageScroll2id">What it's about</a>
	</div>

</div>

<div class="row dm-introduction">
	<div class="large-12 column">
		<div class="dm-intro">
			<p>Every summer a group of our most ambitious and driven students join us in our Divemaster Training Program. This group is at the heart of everything that makes Dive World special. You aren’t just learning how to keep divers safe or conduct underwater tours. You will learn how to take people from all backgrounds and turn them into divers.</p>
		</div>
	</div>
</div>

<script type="text/javascript">

	function show(invisible, selfBtn) {
		dmOne.className = ('read-more heightZero');
		dmTwo.className = ('read-more heightZero');
		dmThree.className = ('read-more heightZero');

		dmBone.className = ('dmB');
		dmBtwo.className = ('dmB');
		dmBthree.className = ('dmB');

		invisible.className = ('read-more');
		selfBtn.className = ('dmB active')
	}

	dmOne = Document.getElementById('dmOne');
	dmTwo = Document.getElementById('dmTwo');
	dmThree = Document.getElementById('dmThree');

	dmBone = Document.getElementById('dmBone');
	dmBtwo = Document.getElementById('dmBtwo');
	dmBthree = Document.getElementById('dmBthree');

</script>

<div class="row need-to-know" id="needToKnow">
	<div class="large-12 column">
		<h2>Here’s what you need to know</h2>
	</div>
	<div class="large-4 columns">
		<img src="/wp-content/uploads/2018/04/IMG_8570_preview.jpg" alt="" class="dm-three-img">
		<h3>Unlimited Diving and Training</h3>
		<p>You will be in the water as often as your schedule allows all summer long.</p>
		<div class="readmore">
			<span onclick="show(dmOne, dmBone)" id="dmBone" class="dmB">Read More</span>
		</div>
	</div>
	<div class="large-4 columns">
		<img src="/wp-content/uploads/2018/04/IMG_8570_preview.jpg" alt="" class="dm-three-img">
		<h3>Broad Diving Curriculum</h3>
		<p>Full face masks, sidemount, adaptive techniques, cold water, warm water, you will gain experience at all aspects.</p>
		<div class="readmore">
			<span onclick="show(dmTwo, dmBtwo)" id="dmBtwo" class="dmB">Read More</span>
		</div>
	</div>
	<div class="large-4 columns">
		<img src="/wp-content/uploads/2018/04/IMG_8570_preview.jpg" alt="" class="dm-three-img">
		<h3>Experienced Mentors</h3>
		<p>Work with mentors that have experience in the most high paced, competitive dive destinations as well as darkest, most difficult conditions.</p>
		<div class="readmore">
			<span onclick="show(dmThree, dmBthree)" id="dmBthree" class="dmB">Read More</span>
		</div>
	</div>
	<div class="large-12 column">
		<!--  -->
		<div class="read-more heightZero" id="dmOne">
			<p>Unlike other courses, the Divemaster program is a very flexible internship. Every Divemaster Trainee has a unique schedule that is designed by their mentor to efficiently accomplish their goals while also fitting into their schedule. Most of our divemasters work a full time job and complete their training exclusively on weekends and evenings. The course requires a minimum of 14 days spread over the season however it is recommended that you join us for as many weekends as you are available for!</p>
		</div>
		<!--  -->
		<div class="read-more heightZero" id="dmTwo">
			<p>While most Divemasters focus on one or two aspects of diving (ie. leading and assisting new divers), good Divemasters have experience in a wide range of diving skills and styles. Throughout the summer you will have the opportunity to dive with full face masks and side mount gear. You will be able to take a leadership role in diving locally at our training lakes and the beautiful Tobermory/Brockville shipwrecks. All of this is in addition to the standard theory, practical skill, and leadership skills included in all PADI Divemaster programs. </p>
		</div>
		<!--  -->
		<div class="read-more heightZero" id="dmThree">
			<p>While you will work with all of the Dive World team throughout your training, you will develop a close working relationship with Dive World’s two most experienced instructors, Mario and Pete. Mario started Dive World after working as an instructor in both the GTA and South East Asia for 8 years. Before his current role as Dive World’s Director of Training, Pete was the General Manager of one of the largest PADI Career Development Centers in the Caribbean.</p>
		</div>
		<!--  -->
	</div>
</div>

<div class="row">
	<div class="large-12 column">
		<p class="full-fake-img">insert a big inspiring full width image</p>
	</div>
</div>

<div class="row fine-print">
	<div class="large-12 column">
		<h2>The fine print</h2>
	</div>
	<div class="large-6 columns">
		<h3>Included:</h3>
		<ul>
			<li>All tuition fees</li>
			<li>All air fills</li>
			<li>All pool fees</li>
		</ul>
		<h3>Not included:</h3>
		<ul>
			<li>Park/boat fees</li>
			<li>PADI membership fees</li>
			<li>Equipment rental</li>
		</ul>
		<h3>You will need</h3>
		<ul>
			<li>A Divemaster Crew-Pak – $530</li>
		</ul>
		<h3>Intership price</h3>
		<ul>
			<li>$999</li>
		</ul>
	</div>
	<div class="large-6 columns">
		<h3>Prerequisites</h3>
		<ul>
			<li>PADI Advanced Open Water Diver</li>
			<li>PADI Rescue Diver</li>
			<li>Emergency First Response Primary and Secondary Care (CPR and First Aid) training within the past 24 months.</li>
			<li>A medical statement signed by a physician within the last 12 months.</li>
			<li>At least 40 logged dives</li>
			<p>Note that qualifying certifications from other diver training organization may apply – ask your Dive World PADI Instructor.</p>
		</ul>
	</div>
	<div class="large-12 column">
		<p>As a dive professional you are expected to own your basic scuba equipment, including a dive computer, a dive knife, and at least two surface signaling devices. During practical skills exercises, like underwater mapping and search and recovery, you’ll use a compass, floats, marker buoys, lift bags, underwater lights and slates.</p>
		<p>Contact Dive World to get advice about everything you’ll need as a dive pro, we have special pricing for all of our Divemaster candidates.</p>
	</div>
</div>

<div class="register-div">
	<div class="buttons">
		<a href="/courses/become-a-padi-divemaster/padi-divemaster-checkout/" class="reg-now">Enroll</a>
		<a href="tel:+14165033483" class="reg-call">Call Us</a>
	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
