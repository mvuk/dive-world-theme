<?php
/*
Template name: Partners
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row fullWidth slider">
	<div class="about-slider partners">

		<div class="nav-container">
			<div class="navigation">
				<div class="item">
					<a href="/about-us/about-dive-world/">About Dive World</a>
				</div>
				<div class="item">
					<a href="/about-us/meet-the-owner/">Meet the Owner</a>
				</div>
				<div class="item">
					<a href="/about-us/meet-training-director/">Meet the Training Director</a>
				</div>
				<div class="item-active">
					<a href="/partners/">Partners</a>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="row about-content">
	<div class="large-9 column">

		<div class="row title-row">
			<div class="large-12 column">
				<h1>Partners</h1>
			</div>
		</div>

		<div class="row content-row">
			<div class="large-6 column behind-the-mask">
				<a href="http://behind-the-mask.com/" class="partner-link" target="_blank">
					<h2>Behind the Mask</h2>
					<img src="/wp-content/uploads/2015/04/behind-the-mask1.png" class="partner-img">
				</a>
			</div>

			<div class="large-6 column monster-divers">
				<a href="http://www.monsterdivers.com/" class="partner-link" target="_blank">
					<h2>Monster Divers</h2>
					<img src="/wp-content/uploads/2015/04/monster-divers.png" class="partner-img">
				</a>
			</div>

			<div class="large-6 column bay-islands">
				<a href="http://www.dive-utila.com/" class="partner-link" target="_blank">
					<h2>Bay Islands College of Diving</h2>
					<img src="/wp-content/uploads/2015/04/bay-islands-college-of-diving.png" class="partner-img">
				</a>
			</div>

			<div class="large-6 column ocean-fox-cotton-bay">
				<a href="http://oceanfoxcottonbay.com/home.html" class="partner-link" target="_blank">
					<h2>Ocean Fox Cotton Bay</h2>
					<img src="/wp-content/uploads/2015/04/ocean-fox-cotton-bay1.png" class="partner-img">
				</a>
			</div>
			
		</div>

	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
