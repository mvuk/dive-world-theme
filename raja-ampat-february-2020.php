<?php
/*
Template name: raja ampat december 2020
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row tripRow top">
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2018/03/raja-ampat-2020-ship.jpg" alt="">
	</div>
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2018/03/raja-ampat-2020.jpg" alt="">
	</div>
</div>

<div class="row tripIntro">
	<div class="large-12 column">
    <h1>RAJA AMPAT</h1>
		<p class="intro-dates">December 7-18, 2020</p>
		<p class="trip-desc">Samambaia is a magnificent Phinisi, a traditional Indonesian wooden sailing vessel. It was built and designed to offer her guests access to the world's top rated dive sites of Indonesia. You’ll enjoy absolute comfort, safety, and autonomy regardless of the remoteness of the dive site.</p>
		<p class="trip-desc">Indonesia is known as a top destination, it is even said that after you have made your first dive here, your life will never be the same again. From very small critters to giant pelagics. these Indonesian waters, situated all around the "Ring of Fire", offer arguably the richest marine biodiversity in the world.</p>
		<a href="https://dw352.infusionsoft.com/app/manageCart/addProduct?productId=119" class="registerOnline">Register Online</a>
	</div>
</div>

<div class="row tripRow">
	<div class="large-12 column">
		<img src="/wp-content/uploads/2019/03/raja-ampat-december-2020.jpg" alt="">
	</div>
</div>


<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
