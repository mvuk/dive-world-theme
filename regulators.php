<?php
/*
Template name: shop/regs
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div id="content" class="large-12 columns">
	<div class="mobile-cat">
		<div class="cat-title"><h1>Regulators</h1></div>

		<div class="cat-list">

			<div class="cat-item">
				<a href="https://diveworld.ca/product-category/scuba/regulators/regulator-sets/" class="cat-top-level"><h2>Complete Sets<i class="fa fa-chevron-right cat-right"></i></h2></a></div>
			<div class="cat-item">
				<a href="https://diveworld.ca/product-category/scuba/regulators/first-and-second-stage-regulators/" class="cat-top-level"><h2>First & Second Stage Sets<i class="fa fa-chevron-right cat-right"></i></h2></a>
			</div>
			<div class="cat-item">
				<a href="https://diveworld.ca/product-category/scuba/regulators/first-stage-regulator/" class="cat-top-level"><h2>First stages<i class="fa fa-chevron-right cat-right"></i></h2></a>
			</div>
			<div class="cat-item">
				<a href="https://diveworld.ca/product-category/scuba/regulators/second-stage-regulator/" class="cat-top-level"><h2>Second stages<i class="fa fa-chevron-right cat-right"></i></h2></a>
			</div>

		</div>

	</div>

</div>


<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
