<?php
/*
Template name: Specialty Training
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div  class="page-wrapper page-left-sidebar">
<div class="row">

<div id="content" class="large-9 right columns course-template" role="main">
	<div class="page-inner">
			<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template();
					?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="large-12 column specialty">
	<p>Specialty training allows you to decide what kind of diver you want to be! </p>
	<p>Select the specialty that excites you most and focus on developing those skills specifically. Each unique specialty opens a wider range of dive sites and adventures.</p>
	<p>We focus on ensuring you have the confidence and knowledge to perform these dives safely.</p>
</div>

<div class="large-12 column specialty">
	<h2>Deep Specialty: $261</h2>
	<p><em>What's involved:</em> 3 dives</p>
	<p>Throughout this course you will get progressively deeper until hitting a maximum depth of 40m/130ft which becomes your new maximum certified depth. Focusing on how to correctly handle gas narcosis, dive planning, and some more advanced safety procedures makes this course an excellent skill builder even for those who don't plan on diving beyond 30m regularly.</p>
	<p><strong>Mandatory Gear:</strong> Dive computer</p>
</div>

<div class="large-12 column specialty">
	<h2>Night Specialty: $261</h2>
	<p><em>What's involved:</em> 3 dives</p>
	<p>We will learn how to safely plan and execute a dive at night paying special attention to unique safety procedure, environmental orientation, underwater communication and hand signals.</p>
	<p><strong>Mandatory Gear:</strong> Primary & back up light, compass</p>
</div>

<div class="large-12 column specialty">
	<h2>Wreck Specialty: $311</h2>
	<p><em>What's involved:</em> 4 dives</p>
	<p>We start by learning to control trim, use a reel, and enter and exit a shipwreck safely. This culminates in an actual penetration dive certifying you to explore the inside of ship wrecks.</p>
	<p><strong>Mandatory Gear:</strong> 150Ft Reel, Underwater light, Underwater slate</p>
</div>

<div class="large-12 column specialty">
	<h2>Underwater Navigator Specialty: $261</h2>
	<p><em>What's involved:</em> 3 dives</p>
	<p>This is an excellent course for divers of all levels. Focusing on both natural navigation (using references or landmarks) and compass navigation this course gives you the skills to navigate waters in various conditions. Utilizing the skills you learn in this course will prevent any long surface swims back to the boat/exit point and give you the skills and confidence required to dive without a guide.</p>
	<p><strong>Mandatory Gear:</strong> 150Ft Reel, Underwater light, Underwater slate</p>
</div>

<div class="large-12 column specialty">
	<h2>Full Face Mask Specialty: $326</h2>
	<p><em>What's involved:</em> 1 Pool Dive 3 Open Water Dives</p>
	<p>We learn the differences involved with wearing a full face scuba mask paying special attention to emergency procedures, how to deal with out of air situations, and what the different features of a full face mask are used for.</p>
	<p><strong>Mandatory Gear:</strong> Full face scuba mask</p>
</div>

<div class="large-12 column specialty">
	<p>*All prices listed are for certified divers who have their own equipment. If you need to rent equipment Dive World has top of the line, brand new equipment available for rent at special rates for our program participants.</p>
	<p>**For divers looking to purchase their own equipment Dive World has special pricing for program participants.</p>
	<p>*** If you own your own equipment that you have not used in a long time, this is the perfect opportunity to have a professional look it over and give you some input. We are here to help!</p>
	<p>**** Does not include charter fees where applicable.</p>
</div>

<!-- PAGE CONTENT ENDS HERE -->

	</div><!-- .page-inner -->
</div><!-- end #content large-9 left -->

<div class="large-3 columns left">
<?php get_sidebar(); ?>
</div><!-- end sidebar -->

</div><!-- end row -->
</div><!-- end page-right-sidebar container -->


<?php get_footer(); ?>
