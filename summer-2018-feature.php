<?php
/*
Template name: summer 2018 feature
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row">
	<div class="large-12 column">
		<h1>Summer 2018 Local Dive Season</h1>
		<p>Another summer, another exciting set of dives. Check out links to our dive outings below and some featured shots from our adventures.</p>
	</div>
</div>

<div class="row event-links">
	<div class="large-3 columns">
		<h4>Tobemory</h4>
		<p>Jun 2 - Jun 3</p>
		<a href="https://www.facebook.com/events/865037216993741/" class="event-link">Facebook event link</a>
	</div>
	<div class="large-3 columns">
		<h4>Tobemory</h4>
		<p>Jun 23 - Jun 24</p>
		<a href="https://www.facebook.com/events/400147740401606/" class="event-link">Facebook event link</a>
	</div>
	<div class="large-3 columns">
		<h4>1000 Islands</h4>
		<p>Jul 14 - Jul 15</p>
		<a href="https://www.facebook.com/events/247571892442323/" class="event-link">Facebook event link</a>
	</div>
	<div class="large-3 columns">
		<h4>Tobemory</h4>
		<p>Jul 28 - Jul 29</p>
		<a href="https://www.facebook.com/events/531476183871664/" class="event-link">Facebook event link</a>
	</div>
	<!--  -->
	<div class="large-3 columns">
		<h4>1000 Islands</h4>
		<p>Aug 11 - Aug 12</p>
		<a href="https://www.facebook.com/events/305071320011139/" class="event-link">Facebook event link</a>
	</div>
	<div class="large-3 columns">
		<h4>Tobemory</h4>
		<p>Sep 1 - Sep 2</p>
		<a href="https://www.facebook.com/events/748661211992993/" class="event-link">Facebook event link</a>
	</div>
	<div class="large-3 columns">
		<h4>Tobemory</h4>
		<p>Sep 22 - Sep 23</p>
		<a href="https://www.facebook.com/events/2052174585054297/" class="event-link">Facebook event link</a>
	</div>
	<div class="large-3 columns">

	</div>
</div>

<div class="summer-featured-photos">
	<div class="row">
		<!--  -->
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A0229.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A0244.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A0366.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A0372.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A0409.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A0558.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A0948.jpg" alt="">
		</div>

		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A1061.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A1118.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/1U0A1146.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/IMG_9319.jpg" alt="">
		</div>

		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/IMG_9327.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/IMG_9334.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/IMG_9340.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/IMG_9375.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/IMG_9406.jpg" alt="">
		</div>

		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/IMG_9417.jpg" alt="">
		</div>
		<div class="large-4 columns">
			<img src="http://diveworld.ca/wp-content/uploads/2018/08/IMG_9466.jpg" alt="">
		</div>

	</div>
</div>

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
