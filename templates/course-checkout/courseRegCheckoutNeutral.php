<?php
/*
Template name: Blank - Course Reg Checkout
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="checkout">

			<!-- <?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?> -->

<!-- PAGE CONTENT STARTS HERE -->

<div class="row header">
	<div class="large-12 column">
		<div class="elements">
			<div class="dw-logo a"></div>
			<div class="dw-phone b">
				<p class="questions">Questions? Call Us</p>
				<p><i class="fa fa-phone" aria-hidden="true"></i> 416-503-3483</p>
			</div>
		</div>
	</div>
</div>

<div class="checkout-bg">

	<!-- <div class="row course-preview-img">
		<div class="large-4 columns">
			<div class="gc-img a"></div>
		</div>
		<div class="large-4 columns">
			<div class="gc-img b"></div>
		</div>
		<div class="large-4 columns">
			<div class="gc-img c"></div>
		</div>
	</div> -->

	<div class="row checkout-form">
			<div class="large-12 column">
				<div class="form">
					<!-- <?php echo do_shortcode('[gravityform id="1" title="true" description="true" ajax="true"]'); ?> -->
					<?php the_field('gravity_form_info'); ?>
				</div>
			</div>
	</div>

</div>
<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
