<?php
/*
Template name: Travel
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row travel">
	<div class="large-12 column">
	<h1 class="travelTitle">Travel</h1>
	</div>
</div>

<div class="row travelRow">

	<!-- <div class="large-6 columns">
		<div class="trip">
				<img src="/wp-content/uploads/2018/02/Utila-Image-2.jpg" alt="">
				<div class="trip-details">
					<h2>UTILA <span class="soldOut">Sold Out!</span></h2>
					<p class="dates">November 17-24, 2018</p>
					<p class="travelShortDesc">Teeming with life, Utila sits right on the edge of the Mesoamerican Reef system. </p>
					<a href="/travel/utila-lodge-november-2018/" class="trip-a">View details</a>
				</div>
		</div>
	</div> -->

	<!-- <div class="large-6 columns">
		<div class="trip">
				<img src="/wp-content/uploads/2018/02/cayman-image-1.jpg" alt="">
				<div class="trip-details">
					<h2>GRAND CAYMAN</h2>
					<p class="dates">February 2-9, 2019</p>
					<p class="travelShortDesc">Quiet resort on a rocky Caribbean shoreline - sure to please even the most avid diver. </p>
					<a href="/travel/grand-cayman-sunset-house-february-2019/" class="trip-a">View details</a>
				</div>
		</div>
	</div> -->

	<div class="large-6 columns">
		<div class="trip">
				<img src="/wp-content/uploads/2018/02/Belize-Image-1.jpg" alt="">
				<div class="trip-details">
					<h2>BELIZE</h2>
					<p class="dates">May 11th-18th 2019</p>
					<p class="travelShortDesc">Belize is a wall diver's paradise - 5 dives per day from a luxurious liveaboard experience.</p>
					<a href="/travel/belize-aggressor-iv-may-2019/" class="trip-a">View details</a>
				</div>
		</div>
	</div>

	<div class="large-6 columns">
		<div class="trip">
				<img src="/wp-content/uploads/2018/03/raja-ampat-2020-ship.jpg" alt="">
				<div class="trip-details">
					<h2>RAJA AMPAT</h2>
					<p class="dates">December 7-18, 2020</p>
					<p class="travelShortDesc">11 night trip on the Samambaia - a magnificent traditional Indonesian wooden sailing vessel. Raja Ampat offers some of the richest biodiversity in the world.</p>
					<a href="/travel/raja-ampat-liveaboard/" class="trip-a">View details</a>
				</div>
		</div>
	</div>

</div>
<!-- <div class="row travelRow">

	<div class="large-6 columns">
		<div class="trip">
				<img src="/wp-content/uploads/2018/03/raja-ampat-2020-ship.jpg" alt="">
				<div class="trip-details">
					<h2>RAJA AMPAT</h2>
					<p class="dates">December 7-18, 2020</p>
					<p class="travelShortDesc">11 night trip on the Samambaia - a magnificent traditional Indonesian wooden sailing vessel. Raja Ampat offers some of the richest biodiversity in the world.</p>
					<a href="/travel/raja-ampat-liveaboard/" class="trip-a">View details</a>
				</div>
		</div>
	</div>

	<div class="large-6 columns">

	</div>


</div> -->

<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
