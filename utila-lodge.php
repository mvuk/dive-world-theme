<?php
/*
Template name: utila lodge
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div id="content" role="main" class="about-template">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; // end of the loop. ?>

<!-- PAGE CONTENT STARTS HERE -->

<div class="row tripRow top">
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2018/02/Utila-Image-1.jpg" alt="">
	</div>
	<div class="large-6 columns">
		<img src="/wp-content/uploads/2018/02/Utila-Image-2.jpg" alt="">
	</div>
</div>

<div class="row tripIntro">
	<div class="large-12 column">
    <h1>UTILA <span class="soldOut">Sold Out!</span></h1>
		<p class="intro-dates">November 17-24, 2018</p>
		<p class="trip-desc">Utila sits right on the edge of the Mesoamerican Reef system. It is the second largest reef in the world after the Great Barrier Reef and stretches over 700 miles from the Yucatan Peninsula to the Bay Islands of Honduras. The reefs at Utila are teeming with life.</p>
		<p class="trip-desc">Utila Lodge is the largest built-out-over-the-water structure on the entire island. No matter where you are on the resort property, the Caribbean is splashing just a few feet below you. The lodge is located right in the center of town which gives you easy access to the local shops and restauruants. That said, since the resort is out over the water, the in-town location does not compromise your privacy or relaxation.</p>
		<!-- <a href="https://dw352.infusionsoft.com/app/orderForms/Dive-World-Utila-Lodge-Nov-2019" class="registerOnline">Register Online</a> -->
	</div>
</div>

<div class="row tripRow">
	<div class="large-12 column">
		<img src="/wp-content/uploads/2018/02/utila-lodge-trip-17-24-nov-2018.jpg" alt="">
	</div>
</div>


<!-- PAGE CONTENT ENDS HERE -->

</div>
<?php get_footer(); ?>
